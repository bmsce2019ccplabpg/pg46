#include <stdio.h>

#include <string.h>

void in1(char[]);

void in2(char[]);

void compute(char[],char[],char[]);
 
void output(char[]);


void in1(char st1[])
    {
        printf("Enter the string");
        gets(st1);
    }

void in2(char st2[])
    {
        printf("Enter the string");
        gets(st2);
    }

void compute(char st1[],char st2[],char st3[])
    {
        int i=0,j=0;
        while(st1[i]!='\0')
        {
           st3[j]=st1[i];
           i++;
           j++; 
        }
        i=0;
        while(st2[i]!='\0')
        {
            st3[j]=st2[i];
            i++;
            j++; 
        }
        st3[j]='\0';
    }

void output(char st3[])
    {
        printf("The concatenated string is %s",st3);
    }
    
int main()
    {
        char st1[100];
        in1(st1);
        char st2[100];
        in2(st2);
        char st3[100];
        compute(st1,st2,st3);
        output(st3); 
        return 0;
    } 