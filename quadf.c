#include <stdio.h>

#include <math.h>

int input ();

float compute (int a, int b, int c);

int check (float n);

int real_equal (int b, int a);

int real_distinct (float n, int b, int a);

int imaginary (float n, int b, int a);


int input ()
{
  int a;
  scanf ("%d", &a);
  return a;

}

float compute (int a, int b, int c)
{
  float D;
  D = (b * b - (4 * a * c));
  return D;


}

int check (float n)
{
  if (n > 0)
    return 1;
  else if (n == 0)
    return 2;
  else
    return 3;



}

int real_distinct (float D, int b, int a)
{
  float r1, r2;
  printf ("the roots are real and distinct\n");

  r1 = ((-b + pow (D, 0.5)) / 2 * a);

  r2 = ((-b - pow (D, 0.5)) / 2 * a);

  printf ("The roots are %f %f", r1, r2);

  return 0;

}

int real_equal (int b, int a)
{
  float r1, r2;

  printf ("The roots are real and equal \n");

  r1 = (-b / (2 * a));

  r2 = r1;

  printf ("the roots are %f,%f", r1, r2);

}

int imaginary (float D, int b, int a)
{
  float m, s;


  D = -D;

  printf ("the roots are imaginary\n");

  m = (-b / (2 * a));

  s = (pow (D, .5) / (2 * a));

  printf ("the roots are %f+i%f and %f-i%f", m, s, m, s);

}

int main ()
{
  int a, b, c, n;
  
  float D;
  
  printf ("enter a");
  
  a = input ();
  
  printf ("enter b");
  
  b = input ();
  
  printf ("enter c");
  
  c = input ();
  
  D = compute (a, b, c);
  
  n = check (D);
  
  if (n == 1)
  
    real_distinct (D, b, a);
  
  else if (n == 2)
  
    real_equal (b, a);
  
  else
  
    imaginary (D, b, a);


}