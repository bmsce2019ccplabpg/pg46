
#include <stdio.h>

int in1();

void in2(int[],int);

int compute(int[],int);

void output(int);

int in1()
    {
        int n;
        printf("Enter the number of elements in an array");
        scanf("%d",&n);
        return n;
    }

void in2(int a[],int n)
    {
        int num;
        for(int i=0;i<n;i++)
        {
            printf("Enter a number: ");
            scanf("%d",&num);
            a[i]=num;
        }
    }

int compute(int a[],int n)
    {
        int val,pos;
        printf("Enter the number to be searched");
        scanf("%d",&val);
        int lb=0,ub=n-1;
        int m;
        while(lb<=ub)
        {
            pos=0;
            m=(lb+ub)/2;
            if(val==a[m])
            {
                pos=m+1;
                break;
            }
            else if(val>a[m])
                lb=m+1;
            else
                ub=m-1;
        }
        return pos;
    }

void output(int pos)
    {
        if(pos>0)
            printf("The given element was found at %d",pos);
        else
            printf("The given element was not found");
    }

int main()
    {
        int n;
        n=in1();
        int a[n];
        in2(a,n);
        int pos;
        pos=compute(a,n);
        output(pos);
        return 0;
    }

