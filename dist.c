#include<stdio.h>
#include<math.h> 

float input ();
float compute (float a, float b, float c, float d);
void output (float a);

float input ()
{
  float a;
  scanf ("%f", &a);
  return a;
}

float compute (float a, float b, float c, float d)
{
  float distance;
  distance = sqrt ((c - a) * (c - a) + (d - b) * (d - b));
  return distance;
}

void output (float a)
{
  printf ("the distance is %f", a);
}

int main ()
{
  float x1, y1, x2, y2, c;
  printf ("enter the x1 of the point:\n");
  x1=input();
  printf ("enter the y1 of the point:\n");
  y1=input();
  printf ("enter the x2 of the point:\n");
  x2=input();
  printf ("enter the y2 of the point:\n");
  y2=input();

  c = compute (x1, y1, x2, y2);
  output (c);
  return 0;
}