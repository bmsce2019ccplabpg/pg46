#include <stdio.h> 

int input1();

void input2(int[],int);

void compute(int[],int);

void output(int[],int);


int input1()
    {
        int n;
        printf("Enter the number of elements in an array");
        scanf("%d",&n);
        return n;
    }
    
void input2(int a[],int n)
    {
        int num;
        for(int i=0;i<n;i++)
        {
            printf("Enter a number: ");
            scanf("%d",&num);
            a[i]=num;
        }
    }
    
void compute(int a[],int n)
    {
        int small=a[0];
        int largest=a[0];
        int sp=0,lp=0;
        for(int i=1;i<n;i++)
        {
            if(a[i]<=small)
            {
                small=a[i];
                sp=i;
            }
        }
        for(int i=1;i<n;i++)
        {
            if(a[i]>=largest)
            {
                largest=a[i];
                lp=i;
            }
        }
        a[sp]=largest;
        a[lp]=small;
    }
    
void output(int a[],int n)
    {
         printf("The changed elements of array are:\n");
         for(int i=0;i<n;i++)
         {
             printf("%d\n",a[i]);
         }  
    }
    
    
int main()
     {
        int n;
        n=input1();
        int a[n];
        input2(a,n);
        compute(a,n);
        output(a,n);
        return 0;
    }
    
