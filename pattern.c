#include <stdio.h>
 
int input();

void output(int a);

int main()
    {
        int a;
        a=input();
        output(a);
        return 0;
    }

int input()
    {
        int a;
        printf("Enter the value of number: ");
        scanf("%d",&a);
        return a;
    }

void output(int a)
    {
        for(int i=1;i<=a;i++)
        {
            for(int j=1;j<=i;j++)
            {
                printf("%d ",j);
            }
            printf("\n");
        }
    }
